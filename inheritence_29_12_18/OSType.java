package adProg.inheritence_29_12_18;

public class OSType extends SmartDevice {

	String osName;

	public OSType(String deviceName, int modelNo, String osName) {

		super(deviceName, modelNo);

		this.osName = osName;
	}
}
