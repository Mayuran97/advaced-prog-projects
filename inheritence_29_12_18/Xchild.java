package adProg.inheritence_29_12_18;

public class Xchild extends Xparent{

	public void callParentMethods() {
		super.displayNum();
	
		System.out.println("Parent age is : " + super.age);
	}
	public static void main(String[] args) {
		Xchild mXchild = new Xchild();
		mXchild.callParentMethods();
	}
}
