package adProg.inheritence_29_12_18;

import java.security.PublicKey;

public class SmartDevice {

	String deviceName;
	int modelNo;

	public SmartDevice(String deviceName, int modelNo) {

		this.deviceName = deviceName;
		this.modelNo = modelNo;
	}

	public void print() {

		System.out.println(deviceName);
		System.out.println(modelNo);
	}
}
