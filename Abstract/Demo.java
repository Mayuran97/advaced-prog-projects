package adprogramming.Abstract;

public class Demo {
	public static void main(String args[]) {

		Person student = new Employee("Dove", "Female", 0);
		Person employee = new Employee("Mayu", "Male", 1997);
		student.work();
		employee.work();

		employee.changeName("Mayuran");
		System.out.println(employee.toString());

	}
}
