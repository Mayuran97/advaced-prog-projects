package adProg.interface16_12_18;

interface A {

	String A = "AAA";
	String methodA();
}

interface B{
	
	String B = "BBB";
	String methodB();
}

class C implements A,B{

	@Override
	public String methodA() {
		// TODO Auto-generated method stub
		return A+B;
	}

	@Override
	public String methodB() {
		// TODO Auto-generated method stub
		return B+A;
	}
}

class D extends C implements A,B{
	
	String D = "DDD";
	
	public String methodA() {
	
		return B+methodB();
	}
}




