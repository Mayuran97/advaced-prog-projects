package bcas.ap.pc;

public class PackMessages {

	public static final String getRowCol = "Enter row & col - eg: 4,3";
	public static final String getToken = "Please enter your token no : ";
	public static final String tokenYes = "Your hole is available";
	public static final String tokenNo = "Your hole is not available";
	public static final String notFree = "This hole is already allocated";
	public static final String success = "Allocated successfully";
}
