package bcas.ap.pc;

public class PackHolder {

	int row,col;
	int holes [][];
	int maxHoles;
	
	public PackHolder(int row, int col) {
		this.row = row;
		this.col = col;
		maxHoles = this.row * this.col;
		createHole();
	}

	private void createHole() {
		holes = new int[row][col];
		
		//Display dis = new Display();
		//dis.drawArray(row, col);
	}
	
	public String checkAvailability(int holeNo) {
		if(holeNo <= maxHoles) {
			return palacePack(holeNo);
		}else {
			return PackMessages.tokenNo;
		}
	}
	
	public String palacePack(int holeNo) {
		int tokenRow = holeNo / col;
		int tokenCol = (holeNo % col) - 1;
		if (checkFree(tokenRow, tokenCol)) {
			holes[tokenRow][tokenCol] = holeNo;
			return PackMessages.success;
		}
		//Display dis = new Display();
		//dis.drawArray(row, col);
		return PackMessages.notFree;
	}
	
	public boolean checkFree(int tokenRow, int tokenCol) {
		holes[3][1] = 20;
		holes[1][4] = 11;
		holes[0][0] = 1;
		
		if(holes[tokenRow][tokenCol] == 0) {
			return true;
		}
	return false;
	}
}







