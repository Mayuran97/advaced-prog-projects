package bcas.ap.pc;

import bcas.ap.utill.ReadValues;

public class PackHolderDemo {
	public static void main(String[] args) {

		ReadValues rv = new ReadValues();
		int[] rowCol = rv.readTwoValues(PackMessages.getRowCol);
		
		PackHolder ph = new PackHolder(rowCol[0], rowCol[1]);
		
		int holeNo = rv.readInt(PackMessages.getToken);
		System.out.println(ph.checkAvailability(holeNo));
		
	}
}
