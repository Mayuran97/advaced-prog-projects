package bcas.ap.utill;

public class Display {

	public void drawArray(int row, int col) {

		int array[][] = new int[row][col];

		for (int x = 0; x < row; x++) {
			for (int y = 0; y < col; y++) {
				System.out.print(array[x][y] + " ");
			}
			System.out.println();
		}
	}
}
