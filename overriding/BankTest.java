package adprogramming.overriding;

public class BankTest {
	public static void main(String[] args) {

		HNB m = new HNB();
		CommercialBank a = new CommercialBank();
		BOC y = new BOC();

		System.out.println("CommercialBank Rate of interest : " + a.getRate_Interest());
		System.out.println("BOC Rate of interest : " + y.getRate_Interest());
		System.out.println("HNB Rate of interest : " + m.getRate_Interest());
	}
}
