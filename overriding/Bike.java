package adprogramming.overriding;

public class Bike extends Vehicle {

	void run() {
		System.out.println("Bike run slowly");

	}

	public static void main(String[] args) {
		Bike obj= new Bike();
		obj.run();
	}
}
