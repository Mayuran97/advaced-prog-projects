package adProg.inheritence;

public class InheritenceDemo{

	public static void main(String[] args) {
		
		Child child = new Child();
		System.out.println(child.addition(5,20));
		System.out.println(child.multiplication(2,10));
		System.out.println(child.subtraction(10,5));
	}
}
