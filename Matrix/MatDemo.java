package dsa.matrix;
import java.util.Scanner;

public class MatDemo {

	public static void main(String args[]) {
		int r1, r2, c1, c2, i, m, n, sum;
		Scanner scan = new Scanner(System.in);

		System.out.println("Enter rows and columns of matrix 1 : ");
		r1 = scan.nextInt();
		c1 = scan.nextInt();

		System.out.println("Enter rows and columns of matrix 2 : ");
		r2 = scan.nextInt();
		c2 = scan.nextInt();

		int mat1[][] = new int[r1][c1];
		int mat2[][] = new int[r2][c2];
		int result[][] = new int[r1][c2];

		System.out.println("Enter the elements of matrix 1 : ");

		for (i = 0; i < r1; i++) {
			for (m = 0; m < c1; m++)
				mat1[i][m] = scan.nextInt();
		}
		System.out.println("Enter the elements of matrix 2 : ");

		for (i = 0; i < r2; i++) {
			for (m = 0; m < c2; m++)
				mat2[i][m] = scan.nextInt();
		}
		System.out.println("****************");
		for (i = 0; i < r1; i++)
			for (m = 0; m < c2; m++) {
				sum = 0;
				for (n = 0; n < r2; n++) {
					sum += mat1[i][n] * mat2[n][m];
				}
				result[i][m] = sum;
			}
		for (i = 0; i < r1; i++) {
			for (m = 0; m < c2; m++)
				System.out.print(result[i][m] + " ");
			System.out.println();
		}
	}
}
